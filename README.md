### 张宁、杜克锐：《效率与生产率分析教程——理论、应用与编程》相关程序代码和数据

* 数据代码已上传，如有问题请联系我们 kerrydu AT xmu.edu.cn

* 本书所有Stata外部命令的安装语句：

  ​       net install kgitee, from(https://gitee.com/kerrydu/kgitee/raw/master) replace

  ​       kgitee efficiencybook,replace force 

  ​       efficiencybook
  
  > 在完成以上安装后，需要下载以下文件：ch7-ch10code&data.zip 是第7-10章节的数据和代码；编程部分程序和代码是Stata编程部分的相关代码和数据。注：需要将数据目录设置为当前工作路径。


> 由于出版仓促，我们发现一些错误，将尽快上传勘误表。还请批评指正！感谢所有读者的支持。

* 勘误表已经上传，感谢读者的反馈，我们将持续更正发现的错误。

PS：gitee不常上来，消息看到会滞后一些，如果有问题可以邮件联系我，谢谢
cap program drop efficiencybook
program define efficiencybook 
di " "
di "Installing packages for efficiencybook..."
di " "

cap which kgitee
if _rc{
	net install kgitee, from(https://gitee.com/kerrydu/kgitee/raw/master) replace
}



kgitee gtfpch, replace force 
kgitee sbmeff, replace force 
kgitee ddfeff, replace force 
kgitee malmq2, replace force 
kgitee dflp,   replace force 
kgitee deadual,replace force 
kgitee sopen, replace force


cap which outreg2

if _rc{
	ssc install outreg2,replace 
}

cap which kdens

if _rc{
	ssc install kdens,replace 
}

cap which simarwilson

if _rc{
	net install  st0585_1.pkg,  from(http://www.stata-journal.com/software/sj20-4/) replace force 
}

cap which  teradial
if _rc {
	net install st0444.pkg, from(http://www.stata-journal.com/software/sj16-3) replace force 
}

cap which  sfpanel 
if _rc{
	net install st0315_2.pkg, from(http://www.stata-journal.com/software/sj15-4) replace force 
}


cap which dea 
if _rc{
	net install st0193.pkg, from(http://www.stata-journal.com/software/sj10-2/)

}

cap which kdens
if _rc{
	ssc install kdens
}


cap which lmoremata.mlib

if _rc{
	ssc install moremata,replace 
}



di " "
di "所有外部命令已经完成安装"_n /*
*/ "请到https://gitee.com/kerrydu/efficiencybook" _n /*
*/ "下载数据和do文件."



end


